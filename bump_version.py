import re
import urllib.request
import json
from subprocess import call

contents = urllib.request.urlopen('https://pypi.python.org/pypi/pyadf/json').read().decode('utf-8')
pkg = json.loads(contents)
releases = pkg['releases']
versions = sorted(releases.keys())
latest = versions[len(versions)-1]

match = re.search("(\d+\.\d+\.)(\d+)", latest)
new_version = match.group(1) + str(int(match.group(2))+1)

print('Attempting to replace version ' + latest + ' with ' + new_version)

content = None
with open ('setup.py', 'r' ) as f:
    content = f.read()
    content = re.sub("version = '\d+\.\d+\.\d+'", "version = '"+new_version+"'", content)

if content is not None:
    with open('setup.py', 'w') as f:
        f.write(content)
        print('Version replaced in setup.py successfully, commiting version bump')

    call(['git', 'commit', '-m', 'Automatic version bump [skip ci]', '-a'])
    call(['git', 'push', 'origin', 'master'])

else:
    print("Could not replace verion")